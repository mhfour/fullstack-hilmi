const mongoose = require('mongoose');

mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
    if (!err) { console.log('Successfully connected to MongoDB'); }
    else { console.log('Error connecting to MongoDB: ' + JSON.stringify(err, undefined, 2)); }
});

require('./user.model');